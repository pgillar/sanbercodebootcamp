//SOAL NO. 1 - Looping While
//LOOPING PERTAMA - while-loop
var kode = 2;
while(kode<=20){
    console.log(kode + " - I love coding");
    kode++;
}
//LOOPING KEDUA - while-loop
var cinta = 20;
while(cinta>=2){
    console.log(cinta + " - I will become a mobile developer");
    cinta--;
}
//SOAL NO. 2 - Looping menggunakan for
//LOOPING KETIGA - for-loop
for(let i=1;i<21;i++){
    //syarat C gabung di 1 kondisi saja karna reqnya sama ganjil:
    //i%2==1 utk imp ganjil, i%3 !=0 utk kondisi kelipatan 3 dan ganjil (!=0 / NOTgenap)
    if (i%2==1 && i%3 !=0) {
        console.log(i+" - santai");
    } else if (i%2==0) {
        console.log(i+" - berkualitas");
    } else{
        console.log(i+" - I Love Coding");
    }
}
//SOAL NO. 3 - Membuat Persegi Panjang
//LOOPING KEEMPAT - for-loop
for (let hash = 1; hash <= 4; hash++) {
    console.log("########")
}
//SOAL NO. 4 - 4 Membuat Tangga
//LOOPING KELIMA - for-loop
for (let i = 3; i >=0; i--) {
    if (i==3) {
        console.log("#");
        console.log("##");
        console.log("###");
        console.log("####");
    } else if (i==2) {
        console.log("#####");
        console.log("######");
        console.log("#######");
    }else{}
}
//SOAL NO. 5 - Membuat Papan Catur
//LOOPING ENAM - for-loop
for (let catur = 1; catur <= 8; catur++) {
    if (catur%2==1) {
        console.log(" # # # #")
    } else if (catur%2==0) {
        console.log("# # # #")
    }else{}
}